import Link from 'next/link'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'


export default function Home(props) {
  return (
    <div className={styles.container}>
      <Head>
        <title>about | Next</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      
      <div class="topnav">
      <Link href="/"><a class="active" >Home</a></Link>
      <Link href="news"><a >News</a></Link>
      <Link href="contact"><a >Contact</a></Link>
      <Link href="about"><a >About</a></Link>
</div>

<div className='about-data'>
  <h1 >About</h1>
  <h3>Welcome to Augment Systems</h3>
    <p>We provide technology consultancy and outsourced product development. Our core strengths lie in product conceptualization, design, development, and leverage of emerging technologies.We offer a complete range of software and consulting services such as CRM solutions, custom application development, system integration and web development, as well as Embedded Solutions</p>
    <h3>Corporate Profile</h3>
    <p>We provide technology consultancy and outsourced product development. Our core strengths lie in product conceptualization, design, development, and leverage of emerging technologies. We offer a complete range of software and consulting services such as CRM solutions, custom application development, system integration and web development, as well as Embedded Solutions.</p>
    <h1><Link href="http://www.augmentsys.com"><a>About More...</a></Link></h1>   

    <h1><Link href="http://www.augmentsys.com/team.php"><a>Team Member</a></Link></h1>   
</div>
<style global js>{`
        body {
          margin: 0;
          font-family: Arial, Helvetica, sans-serif;
        }
        .about-data{
            text-align: center;
            border: 3px solid green;
            margin: 10vh 0vh;

 

        }
        
        .topnav {
          overflow: hidden;
          background-color: #333;
        }
        
        .topnav a {
          float: left;
          color: #f2f2f2;
          text-align: center;
          padding: 14px 16px;
          text-decoration: none;
          font-size: 17px;
        }
        
        .topnav a:hover {
          background-color: #ddd;
          color: black;
        }
        
        .topnav a.active {
          background-color: #04AA6D;
          color: white;
        }
      `}</style>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  )
}


